package com.nespresso.exercise.waiter;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exercise.waiter.factrory.TableFactory;

public class Restaurant {
	private Map<Integer, Table> listeTable;

	public int initTable(int sizeOfTable) {
		listeTable=new HashMap<Integer, Table>();
		Integer idTable = listeTable.size();
		listeTable.put(idTable, TableFactory.createTable(sizeOfTable));
		return idTable;
	}

	public void customerSays(int tableId, String message) {
		Table table=listeTable.get(tableId);
		table.addCommande(message);
	}

	public String createOrder(int tableId) {
		return listeTable.get(tableId).getCommandesFormatee();
	}
}

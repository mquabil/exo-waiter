package com.nespresso.exercise.waiter.exception;

public class RestaurantException extends Exception {

	private String codeException;

	public RestaurantException(String codeException) {

		this.codeException = codeException;
	}

	public String getCodeException() {
		return codeException;
	}
	
	
}

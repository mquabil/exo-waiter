package com.nespresso.exercise.waiter;

public class Commande {

	private String nomClient;
	private String nomCammande;

	public Commande(String nomClient, String nomCamande) {
		this.nomClient=nomClient;
		this.nomCammande=nomCamande;
	}

	public String getNomCammande() {
		return nomCammande;
	}

	public String getNomClient() {
		return nomClient;
	}
	
	
}

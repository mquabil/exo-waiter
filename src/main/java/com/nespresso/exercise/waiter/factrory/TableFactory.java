package com.nespresso.exercise.waiter.factrory;

import com.nespresso.exercise.waiter.Table;

public class TableFactory {

	public static Table createTable(int nombreDePlace) {
		Table result = new Table(nombreDePlace);
		return result;
	}
}

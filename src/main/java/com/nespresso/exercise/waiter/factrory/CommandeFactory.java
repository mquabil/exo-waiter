package com.nespresso.exercise.waiter.factrory;

import com.nespresso.exercise.waiter.Commande;

public class CommandeFactory {
	public static String CHAINE_SPLIT = ":";
	public static String MEME_CHOSE_QUE_DERNIER = "Same";

	public static Commande createCommande(String messageCommande, Commande derniereCommande) {
		String[] infos = messageCommande.split(CHAINE_SPLIT);
		String nomClient = infos[0];
		String nomCommande = infos[1].substring(1);
		if (MEME_CHOSE_QUE_DERNIER.equals(nomCommande)) {
			nomCommande = derniereCommande.getNomCammande();
		}
		return new Commande(nomClient, nomCommande);
	}
}

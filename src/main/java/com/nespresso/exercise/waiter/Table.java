package com.nespresso.exercise.waiter;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exercise.waiter.exception.RestaurantException;
import com.nespresso.exercise.waiter.factrory.CommandeFactory;

public class Table {

	private Map<Integer, Commande> commandes;
	private int nombrePlasse;

	private static final String separator = ", ";
	private static final String MISSING_2 = "MISSING 2";

	public Table(int nombreDePlace) {
		nombrePlasse = nombreDePlace;
		commandes = new HashMap<Integer, Commande>();
	}

	public void addCommande(String messageCommande) {
		Commande commande=CommandeFactory.createCommande(messageCommande, getDerniereCommande()); 
		Integer indexCommande=getCommandeForClient( commande.getNomClient());
		if (indexCommande==0){
			indexCommande=commandes.size();
		}
		commandes.put(indexCommande,commande) ;
	}

	private Integer getCommandeForClient(String nomClient) {
		for (Integer indexClient : commandes.keySet()) {
			if(commandes.get(indexClient).getNomClient().equals(nomClient)){
				return indexClient;
			}
			
		}
		return 0;
	}

	public Commande getDerniereCommande() {
		int indexDeDernierElement = commandes.size() - 1;
		if (exitCommande()) {
			return commandes.get(indexDeDernierElement);
		} else
			return null;
	}

	private boolean exitCommande() {
		return !commandes.isEmpty();
	}

	public String getCommandesFormatee() {
		try {
			verifierCommandeComplet();
		} catch (RestaurantException e) {
			return e.getCodeException();
		}
		return formatCommandes();
	}

	private void verifierCommandeComplet() throws RestaurantException {
		if (commandes.size() < nombrePlasse) {
			throw new RestaurantException(MISSING_2);
		}
	}

	private String formatCommandes() {
		StringBuffer result = new StringBuffer();
		int indexDerniereCommande = commandes.size() - 1;
		for (int i = 0; i < indexDerniereCommande; i++) {
			result.append(commandes.get(i).getNomCammande());
			result.append(separator);
		}
		// Ajouter le dernier Element
		result.append(commandes.get(indexDerniereCommande).getNomCammande());
		return result.toString();
	}

}
